export default function() {
  return [
      {title: 'Dominando as artes do JS', pages: 100},
      {title: 'Culinária ao Vivo', pages: 50},
      {title: 'Como fazer seu livro', pages: 150},
      {title: 'UM titulo ordinário', pages: 1570}
    ];
};
